#!/bin/sh

# In ~/.ssh/config, include the following
#
# Host emacs-jabber-www
# HostName web.sourceforge.net
# User SFUSERNAME,emacs-jabber

if [ ! -f index.org ]; then
  echo "Error: must be run in www checkout root" 1>&2
  exit 1
fi

rsync -v *.html emacs-jabber-www:htdocs/
